# Wise2021

CanSat Projekt für das Wintersemester 2021 und Sommersemester 2022.


## Technischer Aufbau

Der CanSat besteht aus zwei Teil-CanSats.


### Oberer CanSat:

- NanoPi Neo Air
- LiPo
- LiPo Shim
- Kamera


### Unterer CanSat:

- Raspberry Pi Zero W
- LiPo
- LiPo Shim
- MPU9250 (Akzelerometer, Gyroskop, Magnetometer)
- BMP280 (Temperatur, Luftdruck)
- RFM95W (LoRa)
- NEO-6M GPS PositionModul


### Groundstation
- Raspberry Pi 3B+
- beliebiger Computer mit Serial Studio

## Software Aufbau
- files/groundstation: Software für den Raspberry Pi der Groundstation
- files/nano: Software für den Nano Pi des oberen CanSats
- files/rasp: Software für den Raspberry Pi des unteren CanSats
- sandbox: nur für Test- und Probezwecke 

