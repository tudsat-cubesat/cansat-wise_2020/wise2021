import sys
import struct
import random

def foo():
    a = bytes("(10, 20)", "utf-8")
    b = bytes(b"a")
    c = bytes(b"(10, 20)")
    d = bytes(b"18.564,1024.673,9.324678,15.800324,20.546,20.546,20.546,20.546,20.546,20.546,20.546,20.546,20.546")
    e = [(18.564, 1024.673),
         (9.324678, 15.800324), 
         (20.546, 20.546, 20.546, 20.546, 20.546, 20.546, 20.546, 20.546, 20.546)]
    f = [18.564,1024.673,9.324678,15.800324,20.546,20.546,20.546,20.546,20.546,20.546,20.546,20.546,20.546]
    g = struct.pack('f'*len(f), *f)
    for i in [a, b, c, d, e, f, g]:
        print(f"{sys.getsizeof(i)}")
    print(str())
    print(struct.unpack('f'*len(f), g))
        
def bar():
    # latest_values: [(temperature, pressure), (a_x, a_y, a_z, rs_x, rs_y, rs_z, mfd_x, mfd_y, mfd_z), (longitude, latitude, altitude, speed over ground), (signal strength to nano pi)]
    latest_value = [(0.0, 1.0), (2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0), (11.0, 12.0, 13.0, 14.0), (15.0, )]
    # latest_values = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0]
    latest_values = [value for tpl in latest_value for value in tpl]
    print(latest_values)
    #msg = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0]
    msg = struct.pack('f'*len(latest_values), *latest_values)
    print(f"{sys.getsizeof(msg)}")
    print(msg)
    b = struct.unpack('f'*16, msg)
    print(b)
    
def list_to_serial_msg(data: list) -> str:
    return "/*" + ",".join(str(x) for x in data) + "*/"
    
#print(list_to_serial_msg([18.564,1024.673,9.324678,15.800324,20.546,20.546,20.546,20.546,20.546,20.546,20.546,20.546,20.546]))

# [temperature, pressure, a_x, a_y, a_z, rs_x, rs_y, rs_z, mfd_x, mfd_y, mfd_z, longitude, latitude, altitude, speed over ground, signal strength to nano pi]
latest_values = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

for i in range(len(latest_values)):
    latest_values[i] = random.uniform(100.0, 200.0)
    
packed = struct.pack('f'*len(latest_values), *latest_values)
     
print(latest_values)
    
msg = struct.pack('f'*len(latest_values), *latest_values)
ba = bytearray(msg)
print(f"Length of bytearray: {len(ba)}")
print(ba)
unpacked = struct.unpack('f'*len(latest_values), msg)

for i in range(len(latest_values)):
    diff = latest_values[i]-unpacked[i]
    qout = diff / latest_values[i]
    print(f"Diff: {diff}, Verhältnis: {qout}")
    


print(msg)
#bar()
#foo()
     




