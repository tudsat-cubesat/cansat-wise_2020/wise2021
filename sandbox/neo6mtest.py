import serial, pynmea2


port = "/dev/ttyAMA0"
ser = serial.Serial(port, baudrate=9600, timeout=0.5)

while True:
    newdata = ser.readline()
    if newdata[0:6] == b'$GPRMC' or newdata[0:6] == b'$GPGGA':
        parsed = pynmea2.parse(newdata.decode("utf-8"))
        try:
            print(parsed.altitude)
        except:
            continue
        # lat = newmsg.latitude
        # lng = newmsg.longitude
        # gps = "Latitude=" + str(lat) + "and Longitude=" + str(lng)
