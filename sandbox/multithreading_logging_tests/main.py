from threading import Thread, Event
import threads
import logging
import time

def main():
    logging.basicConfig(filename='system_logs.log', level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%Y-%m-%d %H-%M-%S')
    t1 = Thread(target = threads.thread_1, args = (logging, ), name="thread-1")
    t2 = Thread(target = threads.thread_1, args = (logging, ), name="thread-2")
    logging.info("Starting threads... ")
    t1.start()
    t2.start()
    
    while True:
        print("Still running...")
        time.sleep(1)
        
if __name__ == "__main__":
    main()
