import time
from threading import current_thread

def thread_1(logging):
    i = 0
    thread_name = current_thread().getName()
    while True:
        i += 1 
        logging.info(f"{current_thread().getName()}'s i = {i}")
        try:
            a = 1 / 0
        except Exception as e:
            logging.exception(f"{thread_name}: An exception occurred: {e}")
        time.sleep(1)