# https://learn.adafruit.com/lora-and-lorawan-radio-for-raspberry-pi/sending-data-using-a-lora-radio
# https://circuitpython.readthedocs.io/projects/rfm9x/en/latest/api.html
# https://github.com/adafruit/Adafruit_CircuitPython_RFM9x/blob/main/adafruit_rfm9x.py

import time
import board
import busio
from digitalio import DigitalInOut, Direction, Pull
import adafruit_rfm9x
from arg_parser import get_arguments

# Define radio parameters
RADIO_FREQ_MHZ = 433.0

# Configure RFM9x LoRa Radio
CS = DigitalInOut(board.CE1)
RESET = DigitalInOut(board.D25)
spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
rfm96 = adafruit_rfm9x.RFM9x(spi, CS, RESET, RADIO_FREQ_MHZ)
rfm96.tx_power = 23             # transmit power in dBm
rfm96.spreading_factor = 6     # spreading factor, values 6 - 12
rfm96.signal_bandwidth = 20800 # Valid values are listed in (7800, 10400, 15600, 20800, 31250, 41700, 62500, 125000, 250000)


counter = 0
while True:
    counter += 1
    message = bytes(str(counter), "utf-8")
    rfm96.send(message)
    print("Sent message: ",  message)
    time.sleep(1)