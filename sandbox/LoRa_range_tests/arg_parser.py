#!/usr/bin/python

from ast import main
import sys


def get_arguments():
    power = 23
    bandwidth = 20800
    s_factor = 6
    possible_bandwidths = (7800, 10400, 15600, 20800, 31250, 41700, 62500, 125000, 250000)
    for i, arg in enumerate(sys.argv[1:]):
        
        
        if arg[0] == "-":
            try:
                if arg[1] == "p":
                    power_arg = int(sys.argv[i+2])
                    if 0 <= power_arg and power_arg <= 23:
                        power = power_arg
                    else:
                        print(f"The transmit power has to be a value between 0 and 23!")
                        print(f"Using default power: {power}")
                        print("\n")
            except ValueError:
                print(f"Specified argument was not a number: {sys.argv[i+2]}")
                print("\n")

            
            try:
                if arg[1] == "b":
                    bandwidth_arg = int(sys.argv[i+2])
                    if bandwidth_arg in possible_bandwidths:
                        bandwidth = bandwidth_arg
                    else:
                        print(f"The signal bandwidth has to be a value of")
                        print(possible_bandwidths)
                        print(f"Using default signal bandwidth: {bandwidth}")
                        print("\n")
            except ValueError:
                print(f"Specified argument was not a number: {sys.argv[i+2]}")
                print("\n")
                

            try:
                if arg[1] == "s":
                    s_factor_arg = int(sys.argv[i+2])
                    if 6 <= s_factor_arg and s_factor_arg <= 12:
                        s_factor = s_factor_arg
                    else:
                        print(f"The spreading factor has to be a value between 6 and 12")
                        print(f"Using default signal bandwidth: {s_factor}")
                        print("\n")
            except ValueError:
                print(f"Specified argument was not a number: {sys.argv[i+2]}")
                print("\n")
            

    print("\n")
    print(f"Transmit power: {power:>8}")
    print(f"Signal bandwidth: {bandwidth:>6}")
    print(f"Spreading factor: {s_factor:>6}")
    print("\n")
    
    return power, bandwidth, s_factor

if __name__ == "__main__":
    get_arguments()