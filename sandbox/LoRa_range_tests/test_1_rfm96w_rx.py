# https://learn.adafruit.com/lora-and-lorawan-radio-for-raspberry-pi/sending-data-using-a-lora-radio
# https://circuitpython.readthedocs.io/projects/rfm9x/en/latest/api.html

import time
import board
import busio
from digitalio import DigitalInOut, Direction, Pull
import adafruit_rfm9x
from arg_parser import get_arguments

# Define radio parameters
RADIO_FREQ_MHZ = 433.0

# Configure RFM9x LoRa Radio
CS = DigitalInOut(board.CE1)
RESET = DigitalInOut(board.D25)
spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
rfm96 = adafruit_rfm9x.RFM9x(spi, CS, RESET, RADIO_FREQ_MHZ)
rfm96.tx_power = 23     # transmit power in dBm
prev_packet = None

while True:
    packet = rfm96.receive()
    if packet is None:
        print("Waiting for packets...") 
    else:
        packet_text = str(packet, "utf-8")
        print("RX: " + packet_text)
        
