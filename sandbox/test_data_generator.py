from math import sin, cos



def get_test_data(iteration: int) -> list:
    """ Generates test data.

    Args:
        iteration (int): iteration or timestep of the data

    Returns:
        list: list with test data of length 16, with following order of sensor readings: temp, pres, ax, ay, az, gx, gy, gz, mx, my, mz, long, lat, alt, sog, np
    """
    # temp, pres, ax, ay, az, gx, gy, gz, mx, my, mz, lat, long, alt, sog, np
    data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

    data[11] = 48.688317                    # Latitude default value
    data[12] = 11.541738                    # Longitude default value

    iteration = 0

    while True:
        # Calculate new values
        data[0] = 18.0  + sin(iteration) * 2            # Temperature
        data[1] = 973.6295 + sin(iteration)             # Pressure
        data[2] = sin(iteration)                        # Acceleration X
        data[3] = data[2] + 2                           # Acceleration Y
        data[4] = data[2] + 4                           # Acceleration Z
        data[5] = cos(iteration * 0.0001) * 15          # Gyro X 
        data[6] = data[5] + 1                           # Gyro Y
        data[7] = data[5] + 2                           # Gyro Z 
        data[8] = sin(iteration) * 0.5                  # Magnetometer X
        data[9] = data[8] + 1                           # Magnetometer Y
        data[10] = data[8] + 2                          # Magnetometer Z
        data[11] = data[11] + 0.00002                   # Latitude
        data[12] = data[12] + 0.00002                   # Longitude
        data[13] = sin(iteration * 0.00001) * 1000      # Altitude    
        data[14] = 5 + iteration % 5                    # Speed over ground
        data[15] = -70 + iteration % 6                  # Signal strength to Nano Pi

        return data
        

