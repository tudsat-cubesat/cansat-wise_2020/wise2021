import random

songs = ["Heat Waves", "As It Was", "Stay", "Easy on Me", "Shivers"]

def play(players):
    song_number = getRandomSong()
    song_name = songs[song_number - 1]
    print(song_name + " was number " + str(song_number) + " last year")
    for player in players:
        player.guess()
    print("The song was actually number " + str(song_number))
    winners = getWinner(song_number, players)
    for winner in winners:
        print(winner.name + " wins!")

def getRandomSong():
    return random.randint(1, len(songs))

def getWinner(song_number, players):
    current_winners = list()
    min_dif = 100000
    for player in players:
        player_dif = abs(song_number - player.number)
        if player_dif < min_dif:
            current_winners.clear()
            current_winners.append(player)
            min_dif = player_dif
        elif player_dif == min_dif:
            current_winners.append(player)
    return current_winners

class Player:

    def __init__(self, name):
        self.name = name

    def guess(self):
        self.number = getRandomSong()
        print(self.name + " says the song was number " + str(self.number))

if __name__ == "__main__":
    #Program starts here
    players = list()
    players.append(Player("Alice"))
    players.append(Player("Bob"))
    players.append(Player("Charlie"))
    for i in range(1, 6, 1):
        if(i == 3):
            leaver_idx = random.randint(0, len(players) - 1)
            leaver = players.remove(leaver_idx)
            print(leaver.name + " has left the game.")
            print("")
        print("Round " + str(i))
        play(players)
        print("")