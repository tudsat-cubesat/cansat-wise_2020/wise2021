import random

#The top 20 songs of 2022
songs = ["Heat Waves" , "As It Was", "Stay", "Easy on Me", "Shivers", "First Class", "Big Energy", "Ghost", "Super Gremlin", "Cold Heart", "Wait for U", "About Damn Time", "Bad Habits", "Thats What I Want", "Enemy", "Industry Baby", "ABCDEFU", "Need to Know", "Wasted on You", "Me Porto Bonito"]
number_of_games = 5

def play(players):
    song_number = getRandomSong()
    song_name = songs[song_number - 1]
    print("The song is " + song_name)
    for player in players:
        player.guess(song_name)
    print("The song was actually number " + str(song_number))
    winners = getWinners(song_number, players)
    for winner in winners:
        print(winner.name + " wins!")


def getWinners(song_number, players):
    current_winners = list()
    min_dif = 10000
    for player in players:
        player_dif = abs(song_number - player.number)
        if(player_dif < min_dif):
            current_winners.clear()
            current_winners.append(player)
            min_dif = player_dif
        elif(player_dif == min_dif):
            current_winners.append(player)
    return current_winners

def getRandomSong():
    return random.randint(1, len(songs))

class Player:

    def __init__(self, name, favorite_song):
        self.name = name
        self.favorite_song = favorite_song

    def guess(self, song_name):
        if song_name == self.favorite_song:
            self.number = songs.index(song_name) + 1
            print("This is " + self.name + "'s favorite song!")
        else:
            self.number = getRandomSong()
        print(self.name + " says the song was number " + str(self.number))

if __name__ == "__main__":
    #This happens when we start our programm
    players = list()
    players.append(Player("Alice", "Heat Waves"))
    players.append(Player("Bob", "Shivers"))
    players.append(Player("Charlie", "Baby Shark"))
    for i in range(1, number_of_games + 1, 1):
        if(i == 3):
            leaver = players.pop()
            print(leaver.name + " has left the game!")
            print("")
        print("Round " + str(i))
        play(players)
        print("")

