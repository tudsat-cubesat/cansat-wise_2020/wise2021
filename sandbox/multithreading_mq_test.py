from queue import Queue
from threading import Thread
from time import time

chunk_size = 100
num_data = 100000
start_time = time()

# A thread that produces data
def producer1(out_q):
    for x in range(num_data):
        out_q.put("Thread 1:" + str(x))
          
# A thread that produces data
def producer2(out_q):
    for x in range(num_data):
        out_q.put("Thread 2:" + str((x * 2)))

# A thread that consumes data
def consumer(in_q1, in_q2):
    for x in range(int(num_data / chunk_size)):
        for y in range(chunk_size):
            print(in_q1.get())
        for y in range(chunk_size):
            print(in_q2.get())
    print("--- %s seconds ---" % (time() - start_time))

q1 = Queue()
q2 = Queue()
# creating a thread for each function
trd1 = Thread(target=consumer, args = (q1, q2, ))
trd2 = Thread(target=producer1, args = (q1, ))
trd3 = Thread(target=producer2, args = (q2, ))
trd1.start() # starting the thread 1 
trd2.start() # starting the thread 2
trd3.start()