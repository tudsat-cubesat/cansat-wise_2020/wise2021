'''
UART communication on Raspberry Pi using Pyhton
http://www.electronicwings.com
'''

# To modify the permissions of ttyS0: sudo chmod 666 /dev/ttyS0
# latest_values: [(temperature, pressure), (a_x, a_y, a_z, rs_x, rs_y, rs_z, mfd_x, mfd_y,
# mfd_z), (longitude, latitude, altitude, speed over ground), (signal strength to nano pi)]
import serial
from math import sin, cos
from time import sleep

ser = serial.Serial ("/dev/ttyS0", 9600)    #Open port with baud rate

# temp, pres, ax, ay, az, gx, gy, gz, mx, my, mz, long, lat, alt, sog, np
data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

data[11] = 48.688317                    # Latitude default value
data[12] = 11.541738                    # Longitude default value

counter = 0

while True:
      # Calculate new values
    data[0] = 18.0  + sin(counter) * 2            # Temperature
    data[1] = 973.6295 + sin(counter)             # Pressure
    data[2] = sin(counter)                        # Acceleration X
    data[3] = data[2] + 2                         # Acceleration Y
    data[4] = data[2] + 4                         # Acceleration Z
    data[5] = cos(counter * 0.0001) * 15          # Gyro X 
    data[6] = data[5] + 1                         # Gyro Y
    data[7] = data[5] + 2                         # Gyro Z 
    data[8] = sin(counter) * 0.5                  # Magnetometer X
    data[9] = data[8] + 1                         # Magnetometer Y
    data[10] = data[8] + 2                        # Magnetometer Z
    data[11] = data[11] + 0.00002                 # Latitude
    data[12] = data[12] + 0.00002                 # Longitude
    data[13] = sin(counter * 0.00001) * 1000      # Altitude    
    data[14] = 5 + counter % 5                    # Speed over ground
    data[15] = -70 + counter % 6                  # Signal strength to Nano Pi

    message = ",".join(str(x) for x in data)
    message = "/*" +  message + "*/"

    ser.write(str.encode(message))                #transmit data serially 
    # ser.write(b"/*0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15*/")
    counter += 1
    sleep(1)

