from pickle import TRUE
from queue import Queue
from threading import Thread
import time

chunk_size = 100
num_data = 10000
start_time = time.time()
latest_value = [0,0]

# A thread that produces data
def producer1(out_q):
    for x in range(num_data):
        latest_value[0] = x
        out_q.put("Thread 1:" + str(x))
        time.sleep(0.0001)
          
# A thread that produces data
def producer2(out_q):
    for x in range(num_data):
        latest_value[1] = x * 2
        out_q.put("Thread 2:" + str((x * 2)))
        time.sleep(0.0001)

# A thread that consumes data
def consumer(in_q):
    while True:
        print(in_q.get())
    print("--- %s seconds ---" % (time.time() - start_time))

def sender(val):
    while True:
        print("Latest Value ", val, file = open("output.txt", "a"))
        time.sleep(0.1)

q = Queue()

# creating a thread for each function
trd1 = Thread(target=consumer, args = (q, ))
trd2 = Thread(target=producer1, args = (q, ))
trd3 = Thread(target=producer2, args = (q, ))
trdT = Thread(target=sender, args = (latest_value, ))
trd1.start() # starting the thread 1 
trd2.start() # starting the thread 2
trd3.start()
trdT.start()