import subprocess

def enable_ap() -> None:
    """Disables all wifi connections and enables the access point
    """
    subprocess.run(['wpa_cli', '-iwlan0', 'disable_network', str(0)])
    subprocess.run(['sudo', 'ip', 'link', 'set', 'dev', 'wlan0', 'down'])
    subprocess.run(['sudo', 'ip', 'addr', 'add', '192.168.0.172/24', 'dev', 'wlan0'])
    subprocess.run(['sudo', 'systemctl', 'restart', 'dnsmasq.service'])
    subprocess.run(['sudo', 'systemctl', 'restart', 'hostapd.service'])

enable_ap()