import subprocess, time

current_network = 0

def swap_wifi(net_id):
    subprocess.run(['wpa_cli', '-iwlan0', 'select_network', str(net_id)])
    current_network = net_id

def switch_to_ap():
    subprocess.run(['wpa_cli', '-iwlan0', 'disable_network', str(current_network)])
    subprocess.run(['sudo', 'ip', 'link', 'set', 'dev', 'wlan0', 'down'])
    subprocess.run(['sudo', 'ip', 'addr', 'add', '192.168.0.172/24', 'dev', 'wlan0'])
    subprocess.run(['sudo', 'systemctl', 'restart', 'dnsmasq.service'])
    subprocess.run(['sudo', 'systemctl', 'restart', 'hostapd.service'])

def switch_to_wifi(net_id):
    subprocess.run(['sudo', 'systemctl', 'stop', 'dnsmasq.service'])
    subprocess.run(['sudo', 'systemctl', 'stop', 'hostapd.service'])
    swap_wifi(net_id)

