from queue import Queue
from threading import Thread, Event, currentThread
import time
import random

start_time = time.time()
TESTS_TIME = 10

def test_thread(path, sleep_time, latest_values, start_l, end_l):
        while not stop_event.is_set():
            new_vals = [random.random() for i in range(end_l - start_l)]
            latest_values[start_l:(end_l+1)] = new_vals
            with open(path, "a") as file:
                file.write(str(new_vals) + currentThread().getName() + "\n")
            time.sleep(sleep_time)

def thread_cg_com(latest_values):
    while not stop_event.is_set():
        print(latest_values)
        time.sleep(0.03)

def battery_low():
    if time.time() - start_time > TESTS_TIME:
        return True
    return False

def main():

    latest_values = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    battery_not_low = True
    
    th_bmp280 = Thread(target=test_thread, args = ("./TestData/data_1.txt", 0.05, latest_values, 0, 1, ), name='bmp')
    th_mpu9250 = Thread(target=test_thread, args = ("./TestData/data_2.txt", 0.1, latest_values, 2, 6, ), name='mpu')
    th_neo6m = Thread(target=test_thread, args = ("./TestData/data_3.txt", 0.1, latest_values, 7, 9, ), name='neo')
    th_cc_com = Thread(target=test_thread, args = ("./TestData/data_4.txt", 0.1, latest_values, 10, 11, ), name='cc_com')
    th_cg_com = Thread(target=thread_cg_com, args = (latest_values, ), name='cg_com')

    th_bmp280.start()
    th_mpu9250.start()
    th_neo6m.start()
    th_cc_com.start()
    th_cg_com.start()

    while battery_not_low:
        if battery_low():
            stop_event.set()
            break
        time.sleep(1)

    print("Program finished!")



if __name__ == "__main__":
    stop_event = Event()
    
    main()