import cc_com as cc_com
import sys, logging
from threading import Thread, Event

def main(isSubscriber):
    logging.basicConfig(filename='cc_com_test_logs.log', level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%Y-%m-%d %H-%M-%S')
    if(isSubscriber):
        t1 = Thread(target = cc_com.connect_as_subscriber, args = (logging, ), name="cc_com_sub")
    else:
        t1 = Thread(target = cc_com.connect_as_publisher, args = (logging, ), name="cc_com_pub")
    t1.start()
        

if __name__ == "__main__":
   main(eval(sys.argv[1]))


