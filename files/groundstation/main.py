# https://learn.adafruit.com/lora-and-lorawan-radio-for-raspberry-pi/sending-data-using-a-lora-radio
# https://circuitpython.readthedocs.io/projects/rfm9x/en/latest/api.html

import time
import struct
import serial
import board
import busio
from digitalio import DigitalInOut, Direction, Pull
import adafruit_rfm9x

# Define radio parameters
RADIO_FREQ_MHZ = 433.0

# Packet length of received packet
PACKET_LENGTH = 16

# No data msg
MSG_NO_DATA = "/*,,,,,,,,,,,,,,,1*/"

def list_to_serial_msg(data: list) -> str:
    """ Converts the data list to a string for serial transmission and Serial Studio.

    Args:
        data (list): the data list to convert

    Returns:
        str: string in data format for Serial Studio
    """
    return "/*" + ",".join(str(x) for x in data) + "*/"


def main():
    # Configure RFM9x LoRa Radio
    CS = DigitalInOut(board.CE1)
    RESET = DigitalInOut(board.D25)
    spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
    rfm96 = adafruit_rfm9x.RFM9x(spi, CS, RESET, RADIO_FREQ_MHZ)
    rfm96.tx_power = 23     # transmit power in dBm

    # Configure serial connection
    ser = serial.Serial ("/dev/ttyS0", 9600)    #Open port with baud rate

    while True:

        # Receive and encode icoming packages
        packet = rfm96.receive()
        if packet is None:
            # Serial write empty Data
            msg_for_ser = MSG_NO_DATA
        else:
            encoded_packet = struct.unpack('f'*PACKET_LENGTH, packet)
            # TODO: Hier müssen evtl. noch die GPS-Koordinaten von degrees, decimal minutes zu decimal degrees umgewandelt werden
            # z.B. so:
            #converts degrees, decimal minutes to decimal degrees
            # 
            # def coordinates_to_dg(n):
            #     try:
            #         n_in_gmm = str(float(n))

            #         deg_n = float(n_in_gmm[0:n_in_gmm.find(".")-2])
            #         min_n = float(n_in_gmm[n_in_gmm.find(".")-2:])

            #         n_in_dg = deg_n + min_n/60

            #         n_in_dg = round(n_in_dg, 6)

            #         return n_in_dg
            #     except:
            #         return n
            
            msg_for_ser = list_to_serial_msg(encoded_packet)
        
        ser.write(str.encode(msg_for_ser))
        
if __name__ == "__main__":
    main()
        
