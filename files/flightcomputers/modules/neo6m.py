from typing import ByteString, Tuple
import serial
import time
import string
import pynmea2

# Erklärung/Aufbau der NMEA Sentences: http://aprs.gids.nl/nmea/

# TODO: Diese Testdatei in verwendbares Modul umschreiben


port = "/dev/ttyAMA0"
ser = serial.Serial(port, baudrate=9600, timeout=0.5)

def get_raw_data() -> str:
    """ Collects the GPS raw data of the NEO6M GPS module.

    Returns:
        ByteString: the raw data
    """
    return ser.readline().decode('utf-8')

def parse_gprmc_data(data: str) -> Tuple:
    """ Converts raw bytestring data in wanted data tuple.

    Args:
        data (ByteString): raw gps data (nmea sentences)

    Returns:
        Tuple: data tuple: (longitude, latitude, altitude, speed over ground) 
    """
    msg = pynmea2.parse(data)
    return msg.lon, msg.lat, msg.spd_over_grnd

def parse_gpgga_data(data: str) -> Tuple:
    """ Converts raw bytestring data in wanted data tuple.

    Args:
        data (ByteString): raw gps data (nmea sentences)

    Returns:
        Tuple: data tuple: (longitude, latitude, altitude, speed over ground) 
    """
    msg = pynmea2.parse(data)
    return msg.altitude



#while True: 
#        port = "dev/ttyAMA0"
#        ser = serial.Serial(port, baudrate=9600, timeout=0.5)
#        dataout = pynmea2.NMEAStreamReader()
#        newdata = ser.readline()
#
#        if newdata[0:6] == "$GPRMC":
#                newmsg = pynmea2.parse(newdata)
#                lat = newmsg.latitude
#                lng = newmsg.longitude
#                gps = "Latitude=" + str(lat) + "and Longitude=" + str(lng)



