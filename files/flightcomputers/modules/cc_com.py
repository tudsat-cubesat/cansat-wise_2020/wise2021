import time
import logging
import subprocess
from threading import currentThread, Event
import paho.mqtt.client as mqtt
import modules.utils as utils

stop_event = Event()

logging = None
thread_name = None
isSubscriber = None
latest_values = None
MQTT_SERVER = "192.168.0.172"
CHANNEL = "data"
CSV_PATH="cc_com_data.csv"

def decodeSignalStrength(raw : str):
    data = raw.split()
    data = (data[1].split("=")[1].split("/")[0], data[3].split("=")[1])
    return data


def get_signal() -> str:
    """Returns the Signal level as a string

    Returns:
        Str: The link quality and Signal level
    """
    
    #use 'iwconfig wlan0 | grep "Signal level"'
    proc = subprocess.run(['iwconfig', 'wlan0'], check=True, capture_output=True)
    grep_proc = subprocess.run(['grep', 'Signal level'], input=proc.stdout, capture_output=True)
    return grep_proc.stdout[10 : -3].decode("utf-8")

def on_connect(client, userdata, flags, rc) -> None:
    #Check if connection is successfull
    if(rc == 0):
        logging.info(f"{thread_name}: Successfully connected to Broker")
    else:
        logging.error(f"{thread_name}: Connection to Broker failed with result code " + str(rc))
    if(isSubscriber):
        client.subscribe(CHANNEL)
 
def on_message(client, userdata, msg) -> None:
    try:
        #USED FOR NANOPI
        #data = decodeSignalStrength(str(msg.payload.decode("utf-8")))
        #latest_values[3] = (data[0], )
        #USED FOR ESP32
        data = msg.payload
        utils.save_data(CSV_PATH, data)
    except Exception as e: 
        logging.exception(f"{thread_name}: {e}")

def connect(global_path, _latest_values, _logging, _isSubscriber):
    global isSubscriber
    global logging
    global thread_name
    global CSV_PATH
    global latest_values
    isSubscriber = _isSubscriber
    logging = _logging
    thread_name = currentThread().getName()
    CSV_PATH = global_path + CSV_PATH
    latest_values = _latest_values
    if isSubscriber:
        connect_as_subscriber()
    else:
        connect_as_publisher()

def connect_as_publisher():
    logging.info(f"{thread_name}: Connecting as publisher")
    client = mqtt.Client()
    client.on_connect = on_connect
    client.connect(MQTT_SERVER, 1883, 60)
    client.loop_start()
    while not stop_event.is_set():#ADD DELAY?
        rawData = get_signal()
        client.publish(CHANNEL, rawData)
        utils.save_data(CSV_PATH, decodeSignalStrength(rawData)) #EXCEPTION
    client.loop_stop()
    disconnect(client)

def connect_as_subscriber():
    logging.info(f"{thread_name}: Connecting as subscriber")
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(MQTT_SERVER, 1883, 60)
    client.loop_start()
    while not stop_event.is_set():
        time.sleep(1) #REDUCE DELAY TO BE LOWER THAN  CG_DELAY
    client.loop_stop()
    disconnect(client)

def disconnect(client):
    client.disconnect()
    logging.info(f"{thread_name}: Disconnected from broker")
    
