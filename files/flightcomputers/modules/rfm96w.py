# https://learn.adafruit.com/lora-and-lorawan-radio-for-raspberry-pi/sending-data-using-a-lora-radio
# https://circuitpython.readthedocs.io/projects/rfm9x/en/latest/api.html

import time
from typing import ByteString
import board
import busio
from digitalio import DigitalInOut, Direction, Pull
import adafruit_rfm9x

# Define radio parameters
RADIO_FREQ_MHZ = 433.0

# Configure RFM9x LoRa Radio
def setup():
    CS = DigitalInOut(board.CE1)
    RESET = DigitalInOut(board.D25)
    spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
    rfm96 = adafruit_rfm9x.RFM9x(spi, CS, RESET, RADIO_FREQ_MHZ)
    rfm96.tx_power = 23     # transmit power in dBm
    return rfm96
    

