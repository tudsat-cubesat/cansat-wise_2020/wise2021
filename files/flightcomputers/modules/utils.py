import datetime
import time
import csv

def get_current_time():
    return str(datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")) 

def save_data(path: str, data: tuple) -> None:
    """ Appends the csv file specified in path with the specified data.

    Args:
        path (str): the path to the csv file
        data (tuple): data-tuple which is saved in the csv file
    """
    with open(path, "a") as file:
        writer = csv.writer(file)
        writer.writerow((time.time(), *data))
