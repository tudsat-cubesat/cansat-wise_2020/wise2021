import time

import logging
import os
from threading import Thread, Event 
#import RPi.GPIO as GPIO

import modules.cc_com as cc_com
import modules.utils as utils

DEBUG = True
BAT_INDICATOR_PIN = 4     # pin of gpio that indicates low battery voltage    
STARTUP_TIME = utils.get_current_time()
PATH = './' + STARTUP_TIME + '/'     

def init_logging(path: str) -> 0.0:
    logging.basicConfig(filename=path+'system_logs.log', level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%Y-%m-%d %H-%M-%S')
    logging.info(f"Autostart at internal time {STARTUP_TIME} \n")

def main():
    # Creating the directory for the sensor data 
    os.mkdir(PATH)
    
    init_logging(PATH)

    # Setting up GPIO for battery level monitoring
    #GPIO.setmode(GPIO.BCM)
    #GPIO.setup(BAT_INDICATOR_PIN, GPIO.IN)


    # Defining Threads
    t_cc_comm = Thread(target = cc_com.connect, args = (PATH, None, logging, False, ), name="CC Thread") # thread for cansat cansat (CC) communication 

    # Starting Threads
    t_cc_comm.start()

    
    # Check for battery level every two seconds
    #while GPIO.input(BAT_INDICATOR_PIN) == 1 or DEBUG:
    time.sleep(20)


    logging.warning("The battery voltage has reached a critical level.")
    logging.info("Shutting down threads...")
    cc_com.stop_event.set()
    
    # XXX: Evtl. müssen hier noch mehr Sachen gemacht werden (evtl. mit dem LiPo Shim kommunizieren, um die Stromversorgung zu steuern)
    
    time.sleep(4)
    
    if not DEBUG:
        logging.info("Shutting down Raspberry Pi. Bye!")
        os.system("sudo shutdown now")
    
    
if __name__ == "__main__":
    main()
