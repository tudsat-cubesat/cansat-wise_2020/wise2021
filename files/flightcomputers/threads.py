import logging
from threading import Event
import time
import struct
from threading import current_thread

import modules.utils as utils
import modules.bmp280 as bmp280
import modules.mpu9250 as mpu9250
import modules.neo6m as neo6m
import modules.rfm96w as rfm96w

stop_event = Event()

# Sensor-Threads: daten einlesen, abspeichern, Fehler loggen
# Com-Threads: aktuellsten Wert lesen, wert senden

# TODO: Fehler logging fehlt noch
# TODO: muss noch getestet werden
# TODO: evtl. time sleep machen

# latest_values: [(temperature, pressure), (a_x, a_y, a_z, rs_x, rs_y, rs_z, mfd_x, mfd_y, mfd_z), (longitude, latitude, altitude, speed over ground), (signal strength to nano pi, )]

def thread_bmp280(file_path: str, latest_values: list, logging: logging) -> None:
    """ Collects and saves the data collected by the BMP280 temperature and 
    air pressure sensor in a csv file and updates latest_values[0] with 
    the collected data tuple.

    Args:
        file_path (str): path to the directory where the csv file should be placed
        latest_values (list): the thread-shared latest_values list which is updated on index 0
        logging (logging): logging module
    """
    
    thread_name = current_thread().getName()
    initFailed = False

    #Initialize bmp280
    bus = None
    while (bus == None) and not stop_event.is_set():
        try:
            bus = bmp280.setup()
        except Exception as e:
            if not initFailed:
                logging.exception(f"{thread_name}: {e}")
                initFailed = True

    logging.info(f"{thread_name}: Thread started.")

    #Get bmp280 data
    while not stop_event.is_set():
        try:
            # XXX: REDUNDANT zu thread_mpu_9250
            new_vals = bmp280.get_data(bus)
            latest_values[0] =  new_vals
            utils.save_data(file_path + "temperature_pressure.csv", new_vals)
        except Exception as e:     # XXX: WARNUNG alle Exceptions werden gefangen! Besseres Fehlerhandling nötig!
            logging.exception(f"{thread_name}: {e}")
            
    logging.info(f"{thread_name}: Thread ended.")
    
        

def thread_mpu9250(file_path: str, latest_values: list, logging: logging) -> None:
    """ Collects and saves the data collected by the MPU9250 accelerometer/gyroscope/magnetometer
    in a csv file and updates latest_values[1] with the collected data tuple.

    Args:
        file_path (str): path to the directory where the csv file should be placed
        latest_values (list): the thread-shared latest_values list which is updated on index 1
        logging (logging): logging module
    """
    
    thread_name = current_thread().getName()
    initFailed = False
    
    #Initialize mpu9250
    bus = None
    gyro_sens = None
    accel_sens = None
    while (bus == None) and not stop_event.is_set():
        try:
            bus = mpu9250.setup()
            gyro_sens, accel_sens = mpu9250.init(bus)
        except Exception as e:
            if not initFailed:
                logging.exception(f"{thread_name}: {e}")
                initFailed = True

    logging.info(f"{thread_name}: Thread started.")

    #Get mpu9250 data
    while not stop_event.is_set():
        try:
            # XXX: REDUNDANT zu thread_bmp280
            new_vals = mpu9250.get_data(bus, gyro_sens, accel_sens)
            latest_values[1] =  new_vals
            utils.save_data(file_path + "accel_gyro_mag.csv", new_vals)
        except Exception as e:     # XXX: WARNUNG alle Exceptions werden gefangen! Besseres Fehlerhandling nötig!
            logging.exception(f"{thread_name}: {e}")
    logging.info(f"{thread_name}: Thread ended.")
    

def thread_neo6m(file_path: str, latest_values: list, logging: logging) -> None:
    """ Collects and saves the raw gps data in a csv file and updates latest_values[2] with the collected data tuple
        consisting of (longitude, latitude, altitude, speed over ground).

    Args:
        file_path (str): path to the directory where the csv file should be placed
        latest_values (list): the thread-shared latest_values list which is updated on index 1
        logging (logging): logging module
    """

    thread_name = current_thread().getName()
    logging.info(f"{thread_name}: Thread started.")
    
    while not stop_event.is_set():
        try:
            # XXX: REDUNDANT zu thread_bmp280
            new_vals = neo6m.get_raw_data()
            if new_vals[0:6] == "$GPRMC":
                parsed_gprmc = neo6m.parse_gprmc_data(new_vals)
                latest_values[2] =  (*parsed_gprmc, latest_values[2][3])
            elif new_vals[0:6] == "$GPGGA":
                 parsed_gpgga = neo6m.parse_gpgga_data(new_vals)
                 latest_values[2] =  (*latest_values[2][:3], parsed_gpgga)
            utils.save_data(file_path + "gps.csv", new_vals.split(","))
        except Exception as e:     # XXX: WARNUNG alle Exceptions werden gefangen! Besseres Fehlerhandling nötig!
            logging.exception(f"{thread_name}: {e}")
    logging.info(f"{thread_name}: Thread ended.")
    

# Thread for cansat groundstation communication
def thread_cg_comm(latest_values: list, delay: float, logging: logging) -> None:
    """ Sends the latest values to the groundstation via the LoRa RFM96W radio every delay seconds.

    Args:
        latest_values (list): the latest values that are being sent
        delay (float): the delay between the sent messages in seconds
        logging (logging): logging module
    """
    
    thread_name = current_thread().getName()
    initFailed = False

    #Initialize rfm96
    rfm96 = None
    while (rfm96 == None) and not stop_event.is_set():
        try:
            rfm96 = rfm96w.setup()
        except Exception as e:
            if not initFailed:
                logging.exception(f"{thread_name}: {e}")
                initFailed = True

    logging.info(f"{thread_name}: Thread started.")

    while not stop_event.is_set():
        try:
            latest_values_converted = [value for tpl in latest_values for value in tpl]     # extracts the data out of the tuples into a single list of values
            for i in range(len(latest_values_converted)):
                if not isinstance(latest_values_converted[i], float):
                    latest_values_converted[i] = 0.0
            print(latest_values)
            msg = struct.pack('f'*len(latest_values_converted), *latest_values_converted)   # packs the data into a byte string
            rfm96.send(msg)
            latest_values[3] = (0,) #Reset Connection strenth to 0 
            time.sleep(delay)
        except Exception as e:     # XXX: WARNUNG alle Exceptions werden gefangen! Besseres Fehlerhandling nötig!
            logging.exception(f"{thread_name}: {e}")
            
    
    logging.info(f"{thread_name}: Thread ended.")
    


    
