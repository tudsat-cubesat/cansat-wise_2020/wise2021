import time

#time.sleep(60)      # waiting 60 seconds for booting up

import logging
import datetime
import os
from threading import Thread, Event 
import RPi.GPIO as GPIO

import threads
#import modules.cc_com as cc_com
import modules.utils as utils

DEBUG = True
CG_DELAY = 1            # delay between two transmission of latests values to the groundstation 
BAT_INDICATOR_PIN = 4     # pin of gpio that indicates low battery voltage    
STARTUP_TIME = utils.get_current_time()
PATH = './' + STARTUP_TIME + '/'     

def init_logging(path: str) -> 0.0:
    logging.basicConfig(filename=path+'system_logs.log', level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%Y-%m-%d %H-%M-%S')
    logging.info(f"Autostart at internal time {STARTUP_TIME} \n")

def main():
    
    # Initializing latest values
    latest_values = [(0.0, 0.0), (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0), (0.0, 0.0, 0.0, 0.0), (0.0, )]
    #print(latest_values)

    # Creating the directory for the sensor data 
    os.mkdir(PATH)
    
    init_logging(PATH)

    # Setting up GPIO for battery level monitoring
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(BAT_INDICATOR_PIN, GPIO.IN)


    # Defining Threads
    t_bmp280 = Thread(target = threads.thread_bmp280, args = (PATH, latest_values, logging, ), name="BMP280 Thread")
    t_mpu9250 = Thread(target = threads.thread_mpu9250, args = (PATH, latest_values, logging, ), name="MPU9250 Thread")
    t_neo6m = Thread(target = threads.thread_neo6m, args = (PATH, latest_values, logging, ), name="NEO6M Thread")
    t_cg_comm = Thread(target = threads.thread_cg_comm, args = (latest_values, CG_DELAY, logging, ), name="CG Thread")   # thread for cansat groundstation (CG) communication 
    #t_cc_comm = Thread(target = cc_com.connect, args = (PATH, latest_values, logging, True, ), name="CC Thread") # thread for cansat cansat (CC) communication 

    # Starting Threads
    t_bmp280.start()
    t_mpu9250.start()
    t_neo6m.start()
    t_cg_comm.start()
    #t_cc_comm.start()

    
    # Check for battery level every two seconds
    while GPIO.input(BAT_INDICATOR_PIN) == 1 or DEBUG:
        time.sleep(2)


    logging.warning("The battery voltage has reached a critical level.")
    logging.info("Shutting down threads...")
    threads.stop_event.set()
    #cc_com.stop_event.set()
    
    # XXX: Evtl. müssen hier noch mehr Sachen gemacht werden (evtl. mit dem LiPo Shim kommunizieren, um die Stromversorgung zu steuern)
    
    time.sleep(4)
    
    logging.info("Shutting down Raspberry Pi. Bye!")
    os.system("sudo shutdown now")
    
    
if __name__ == "__main__":
    main()
