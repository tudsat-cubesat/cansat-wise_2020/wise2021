/*********
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-cam-take-photo-save-microsd-card
  
  IMPORTANT!!! 
   - Select Board "AI Thinker ESP32-CAM"
   - GPIO 0 must be connected to GND to upload a sketch
   - After connecting GPIO 0 to GND, press the ESP32-CAM on-board RESET button to put your board in flashing mode
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*********/

#include "esp_camera.h"
#include "Arduino.h"
#include "FS.h"                // SD Card ESP32
#include "SD_MMC.h"            // SD Card ESP32
#include "soc/soc.h"           // Disable brownour problems
#include "soc/rtc_cntl_reg.h"  // Disable brownour problems
#include "driver/rtc_io.h"
#include "EspMQTTClient.h"
#include "camera_pins.h"
#include "WiFi.h"

#define TIME_BETWEEN_PIC 1000 / 2  //Time between pictures in milliseconds
#define TIME_BETWEEN_MSG 1000 / 5     //Time between MQTT-Messages in milliseconds              
#define FRAMESIZE FRAMESIZE_CIF       // FRAMESIZE_ + QVGA|CIF|VGA|SVGA|XGA|SXGA|UXGA (only used for psram!)

/*
EspMQTTClient client(
  "CanSat",
  "TUDSAT2022",
  "192.168.0.172",  // MQTT Broker server ip
  "UpperCanSat"      // Client name that uniquely identify your device
);
*/
int pictureNumber = 0;
u_long timeOfLastPic = 0;
//u_long timeOfLastMsg = 0;


void setup() {
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); //disable brownout detector
  //Serial.begin(115200);
  setupCamera();

}

void loop() {
  //Handeling the Camera
  int delay = millis() - timeOfLastPic;
  if(delay >= TIME_BETWEEN_PIC){
    takeAndSavePic(delay);
  }
  /*
  //Handeling MQTT
  if(millis() - timeOfLastMsg >= TIME_BETWEEN_MSG){
    client.publish("data", String(WiFi.RSSI()));
    timeOfLastMsg = millis();
  }
  if(pictureNumber >= 20){
    finish();
  }
  client.loop();
  */
}
/*
void onConnectionEstablished() {

  client.subscribe("data", [] (const String &payload)  {
    Serial.println(payload);
  });
}
*/
void takeAndSavePic(int delay){

  // Path where new picture will be saved in SD Card
  String path = "/picture" + String(pictureNumber) + "_d_" + delay + ".jpg";

  // Take Picture with Camera
  camera_fb_t* fb = NULL;
  fb = esp_camera_fb_get();  
  if(!fb) {
    Serial.println("Camera capture failed");
    return;
  }

  timeOfLastPic = millis();

  //Serial.printf("Picture file name: %s\n", path.c_str());
  
  // Save picture to SD Card
  fs::FS &fs = SD_MMC; 
  File file = SD_MMC.open(path.c_str(), FILE_WRITE);
  if(!file){
    Serial.println("Failed to open file in writing mode");
  } 
  else {
    file.write(fb->buf, fb->len); // payload (image), payload length
    Serial.printf("Saved file to path: %s\n", path.c_str());
  }
  file.close();
  esp_camera_fb_return(fb); 

  pictureNumber += 1;
}

void setupCamera(){
  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG; 
  
  if(psramFound()){
    config.frame_size = FRAMESIZE; // FRAMESIZE_ + QVGA|CIF|VGA|SVGA|XGA|SXGA|UXGA
    config.jpeg_quality = 10;
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_SVGA;
    config.jpeg_quality = 12;
    config.fb_count = 1;
  }
  
  // Init Camera
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    return;
  }
  rtc_gpio_hold_dis(GPIO_NUM_4);
  
  //Serial.println("Starting SD Card");
  if(!SD_MMC.begin()){
    Serial.println("SD Card Mount Failed");
    return;
  }
  
  uint8_t cardType = SD_MMC.cardType();
  if(cardType == CARD_NONE){
    Serial.println("No SD Card attached");
    return;
  }
}

void finish(){
  //Turns off the ESP32-CAM white on-board LED (flash) connected to GPIO 4
  pinMode(4, OUTPUT);
  digitalWrite(4, LOW);
  rtc_gpio_hold_en(GPIO_NUM_4);

  Serial.println("Going to sleep now");
  delay(2000);
  esp_deep_sleep_start();
}
